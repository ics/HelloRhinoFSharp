﻿namespace HelloRhino

open System

open Rhino
open Rhino.Commands
open Rhino.Geometry
open Rhino.Input
open Rhino.Input.Custom
open Rhino.UI

open Eto.Drawing
open Eto.Forms

type public HelloRhinoCommand() =
    inherit Rhino.Commands.Command()

    override this.EnglishName with get () : string = "HelloRhinoCommand"
 
    override this.RunCommand(doc : Rhino.RhinoDoc, mode : RunMode) : Result =
        RhinoApp.WriteLine(this.EnglishName, "is working")
        Result.Success