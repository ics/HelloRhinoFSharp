﻿namespace HelloRhino

type HelloRhinoPlugin() =
    inherit Rhino.PlugIns.PlugIn()

    member this.Instance : HelloRhinoPlugin = this
