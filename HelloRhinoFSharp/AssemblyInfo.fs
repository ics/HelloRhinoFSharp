﻿namespace HelloRhino

open System.Reflection
open System.Runtime.InteropServices
open Rhino.PlugIns

// Plug-in Description Attributes - all of these are optional
// These will show in Rhino's option dialog, in the tab Plug-ins
[<assembly: PlugInDescription (DescriptionType.Address, "")>]
[<assembly: PlugInDescription (DescriptionType.Country, "")>]
[<assembly: PlugInDescription (DescriptionType.Email, "")>]
[<assembly: PlugInDescription (DescriptionType.Phone, "")>]
[<assembly: PlugInDescription (DescriptionType.Organization, "")>]
[<assembly: PlugInDescription (DescriptionType.UpdateUrl, "")>]
[<assembly: PlugInDescription (DescriptionType.WebSite, "")>]

// Information about this assembly is defined by the following attributes.
// Change them to the values specific to your project.

[<assembly: AssemblyTitle ("HelloRhino")>]
[<assembly: AssemblyDescription ("")>]
[<assembly: AssemblyConfiguration ("")>]
[<assembly: AssemblyCompany ("")>]
[<assembly: AssemblyProduct ("")>]
[<assembly: AssemblyCopyright ("")>]
[<assembly: AssemblyTrademark ("")>]
[<assembly: AssemblyCulture ("")>]

// The assembly version has the format "{Major}.{Minor}.{Build}.{Revision}".
// The form "{Major}.{Minor}.*" will automatically update the build and revision,
// and "{Major}.{Minor}.{Build}.*" will update just the revision.

[<assembly: AssemblyVersion ("1.0.*")>]

// Rhino requires a Guid assigned to the assembly. Xamarin Studio can't insert a Guid in file templates automatically.
[<assembly: Guid ("14244583-ac6c-4089-abb8-028b1f567842")>]

do()
